<?php


namespace magic;


class Browser
{
    private object $cards ;

    public function __construct()
    {
        $data = file_get_contents(PHP_DIR."data/cards.json") ;
        $this->cards = json_decode($data) ;
    }

    public function generateCards(){ ?>
        <form class="browser">
            <div class="browser-content-wrapper">
                <div class="browser-content">
                    <?php foreach($this->cards as $c) :?>
                    <div class="magic-card">
                        <img src="<?php echo $c->image_uris->normal ?>">
                        <legend style="justify-content: center">
                            <label for="<?php echo $c->id ?>"><?php echo $c->name ?></label>
                        </legend>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </form>
    <?php
    }

}