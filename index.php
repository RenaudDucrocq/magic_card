<?php
require_once "config.php" ;

session_start() ;
$logged = isset($_SESSION['nickname']) ;

require "class/Autoloader.php";
Autoloader::register();
use magic\Template ;
?>

<?php ob_start() ?>

<?php if($logged): ?>
<h1>Hi <?php echo $_SESSION['nickname'] ?>, </h1>
<?php endif; ?>

<div class="title">WELCOME TO THE MAGIC STORE</div>

<img src="<?php echo IMG_DIR?>MagicLogo4.png" style="">

<?php $code = ob_get_clean() ?>
<?php Template::render($code);
