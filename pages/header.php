<?php
$logged = isset($_SESSION['nickname']) ;
?>

<header>
    <nav class="navbar navbar-dark bg-dark">
        <div class="nav-content">
            <a class="navbar-brand" href="<?php echo DOCUMENT_DIR ?>index.php">
                <div style="width: 0.5em"></div>
                <h1>Magic Store</h1>
            </a>
            <a class="btn btn-dark" role="button" href="<?php echo DOCUMENT_DIR ?>pages/browse.php">
                Browse
            </a>

            <div style="flex: 1"></div>

            <div class="btn-group" role="group">
            </div>

        </div>
    </nav>
</header>
