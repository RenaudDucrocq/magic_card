<?php
require __DIR__."/../config.php" ;

session_start() ;

require PHP_DIR."class/Autoloader.php" ;
Autoloader::register();
use magic\Template ;
use magic\Browser ;

$browser = new Browser() ;
?>

<?php ob_start() ?>

<header style="margin: 15px">
    <div>Magic Cards</div>
</header>

<?php $browser->generateCards() ?>

<?php $code = ob_get_clean() ?>
<?php Template::render($code); ?>